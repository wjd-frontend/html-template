jQuery(document).ready(function () {
    /**
     * Global Vars
     */
    var body = jQuery('body');
    /**
     * Apply IE Styles
     */
    if (getInternetExplorerVersion() != -1){
        jQuery('body').addClass('ie');
        if (jQuery(window).width() > 768) {
            var cards = jQuery('.card');
            cards.each(function(k, value) {
                value = jQuery(value);
                imagesLoaded(value, function() {
                    value = value.find('img')
                    if (value.length > 0) {
                        var valueParent = value.parent();
                        if (value.outerWidth() / value.outerHeight() > valueParent.outerWidth() / valueParent.outerHeight()) {
                            value.css('max-height', '100%');
                        } else {
                            value.css('max-width', '100%');
                            valueParent.css('min-height', value.outerHeight());
                        }
                    }                    
                })
            })
        }
    }
    /**
     * Nav Menus
     */
    var mainNavItems = jQuery('.main-nav__item.menu-item-has-children');
    var subNav = jQuery('.header__sub-nav-wrapper');
    if (mainNavItems.length > 0) {
        mainNavItems.click(function(e) {
            var target = jQuery(e.target);
            var link = target.find('>a');
            var clickedIndex = target.index() + 1;
            if (link.hasClass('is-selected')) {
                //Only Collapse current Menu
                link.removeClass('is-selected');
                subNav.removeClass('is-open');
                subNav.find('.is-selected').removeClass('is-selected');
            } else {
                //Check if there is another menu open, if so close it
                if (mainNavItems.find('.is-selected').length > 0) {
                    mainNavItems.find('.is-selected').removeClass('is-selected');
                    subNav.find('.is-selected').removeClass('is-selected');
                }
                //Open current menu
                subNav.addClass('is-open');
                link.addClass('is-selected');
                subNav.find('.sub-nav:nth-child('+clickedIndex+')').addClass('is-selected');
            }
        });
        jQuery('.header__close').click(function() {
            mainNavItems.find('.is-selected').removeClass('is-selected')
            subNav.removeClass('is-open');
            subNav.find('.is-selected').removeClass('is-selected');
        })
    }
    var mobileNav = jQuery('.mobile-nav');
    if (mobileNav.length > 0) {
        var jsHeader = jQuery('.js-header');
        var headerToggle = jQuery('.header__toggle');
        headerToggle.click(function() {
            if (jsHeader.hasClass('is-open')) {
                jsHeader.removeClass('is-open');
                jsHeader.addClass('is-closed');
                headerToggle.removeClass('is-active');
            } else {
                jsHeader.removeClass('is-closed');
                jsHeader.addClass('is-open');
                headerToggle.addClass('is-active')
            }
        })
        var menuLinks = mobileNav.find('li.mobile-nav__item');
        menuLinks.each(function(k, value) {
            value = jQuery(value);
            var valueLink = value.find('>a');
            value.click(function(e) {
                var target = jQuery(e.target);
                e.stopPropagation();
                if (value.hasClass('is-not-selected')) {
                    value.removeClass('is-not-selected');
                    valueLink.removeClass('is-not-selected');
                    value.addClass('is-selected');
                    valueLink.addClass('is-selected');
                    jQuery('body, html').animate({
                        scrollTop: value.offset().top
                    }, 400);
                } else {
                    if (target.hasClass('is-selected')) {
                        value.removeClass('is-selected');
                        valueLink.removeClass('is-selected');
                        value.addClass('is-not-selected');
                        valueLink.addClass('is-not-selected');
                    }
                }
            })
        })
    }
    /**
     * Hero Swiper
     */
    var heroSwiper = jQuery('.hero-swiper');
    if (heroSwiper.length > 0) {
        if (heroSwiper.find('.swiper-slide').length > 1) {
            var mySwiper = new Swiper('.hero-swiper', {
                direction: 'horizontal',
                loop: true,
                pagination: {
                    el: '.hero__pagination',
                        clickable: false,
                        renderBullet: function (index, className) {
                            return '<li class="hero__pagination-item ' + className + '"></li>';
                        }
                }
            })
        }
    }
    /**
     * Image Gallery Lightbox
     */
    var imageGallery = jQuery('.image-gallery.is-desktop');
    if (imageGallery.length > 0) {
        imageGallery.each(function(k, swiperValue) {
            swiperValue = jQuery(swiperValue);
            swiperValue.find('.gallery-swiper').addClass('gallery-swiper-'+k);
            var overlay = swiperValue.find('.image-gallery-overlay');
            var gallerySwiper = new Swiper('.gallery-swiper-'+k, {
                direction: 'horizontal',
                loop: true,
            });
            swiperValue.find('.image-gallery__thumbnail').each(function(k, value) {
                value = jQuery(value);
                value.click(function() {
                    var openIndex = value.data('index-image');
                    gallerySwiper.slideTo(openIndex,0,false);
                    overlay.removeClass('invis');
                    body.addClass('is-modal-open');
                })
            })
            swiperValue.find('.image-gallery__thumbnail-redirect').click(function() {
                swiperValue.find('.image-gallery__thumbnail').first().trigger('click');
            })
            overlay.find('.image-gallery-overlay__close').click(function() {
                console.log('test');
                overlay.addClass('invis');
                body.removeClass('is-modal-open');
            })
            overlay.find('.image-gallery-overlay__next').click(function() {
                gallerySwiper.slideNext();
            })
            overlay.find('.image-gallery-overlay__prev').click(function() {
                gallerySwiper.slidePrev();
            })
        })
    }
    var mobileImageGallery = jQuery('.image-gallery.is-mobile');
    if (mobileImageGallery.length > 0) {
        mobileImageGallery.each(function(k, mobGalValue) {
            mobGalValue = jQuery(mobGalValue);
            mobGalValue.find('.swiper-container').addClass('gallery-swiper-'+k);
            mobGalValue.find('.toggle').click(function(e) {
                e.preventDefault();
                var slidesJSON = JSON.parse(mobGalValue.attr('data-images'));
                var swiperWrapper = mobGalValue.find('.swiper-wrapper');
                swiperWrapper.empty();
                jQuery.each(slidesJSON, function(k, JSONvalue) {
                    if (JSONvalue.src) {
                        swiperWrapper.append('<div class="image-gallery__slide swiper-slide"><div class="image-gallery__thumbnail"><figure class="figure"><div class="figure__image-wrapper is-highlighted"><img src="'+JSONvalue.src+'" class="figure__image"></div><figcaption class="figure__caption"><div class="image-gallery__caption caption"><div class="image-gallery__caption-meta caption__meta">'+JSONvalue.meta+'</div><div class="image-gallery__caption-title caption__title">'+JSONvalue.caption+'</div><div class="image-gallery__caption-credit caption__credit">©'+JSONvalue.credit+'</div></div></figcaption></figure></div></div>');
                    } else {
                        swiperWrapper.append('<div class="image-gallery__slide swiper-slide"><div class="image-gallery__thumbnail"><figure class="figure"><div class="figure__image-wrapper is-highlighted"><div class="figure__image">'+JSONvalue.image+'</div></div><figcaption class="figure__caption"><div class="image-gallery__caption caption"><div class="image-gallery__caption-meta caption__meta">'+JSONvalue.meta+'</div><div class="image-gallery__caption-title caption__title">'+JSONvalue.caption+'</div><div class="image-gallery__caption-credit caption__credit">©'+JSONvalue.credit+'</div></div></figcaption></figure></div></div>');
                    }
                })
                var mySwiper = new Swiper('.gallery-swiper-'+k, {
                    direction: 'horizontal',
                    loop: true,
                });
            })
        })
    }
    /**
     * JS Webforms
     */
    var jsWebform = jQuery('.js-webform');
    if (jsWebform.length > 0) {
        jsWebform.each(function(k, formValue) {
            formValue = jQuery(formValue);
            var innerForm = formValue.find('form');
            var requiredInputsMap = [];
            innerForm.find('input[required]').each(function(k, reqValue) {
                reqValue = jQuery(reqValue);
                requiredInputsMap.push({value: reqValue, name: reqValue.attr('name')})
                reqValue.removeAttr('required');
            });            
            innerForm.find('textarea[required]').each(function(k, reqValue) {
                reqValue = jQuery(reqValue);
                requiredInputsMap.push({value: reqValue, name: reqValue.attr('name')})
                reqValue.removeAttr('required');
            });
            innerForm.submit(function(e) {
                e.preventDefault();
                var failedInputs = [];
                innerForm.find('.is-invalid').removeClass('is-invalid')
                jQuery(requiredInputsMap).each(function(key) {
                    if (requiredInputsMap[key].value.attr('type') === 'radio') {
                        var sameNameFilter = requiredInputsMap.filter(function(x) {
                            return x.name === requiredInputsMap[key].name;
                        });
                        var isChecked = false;
                        jQuery(sameNameFilter).each(function(k, value) {
                            if (value.value.is(':checked')) {
                                isChecked = true;
                                return false;
                            }
                        })                        
                        if (!isChecked) {
                            requiredInputsMap[key].value.closest('.form-item').addClass('is-invalid');
                            requiredInputsMap[key].value.closest('.js-webform-radios').addClass('is-invalid');
                            failedInputs.push(requiredInputsMap[key].value.data('name'));
                        }
                    } else if (requiredInputsMap[key].value.attr('type') === 'checkbox') {
                        if (requiredInputsMap[key].value.is(':checked')) {
                            return false;
                        } else {
                            requiredInputsMap[key].value.closest('.js-form-type-checkbox').addClass('is-invalid');
                            failedInputs.push(requiredInputsMap[key].value.data('name'));
                        }
                    } else {
                        if (requiredInputsMap[key].value.val().length <= 0) {                            
                            requiredInputsMap[key].value.closest('.form-item').addClass('is-invalid');
                            failedInputs.push(requiredInputsMap[key].value.data('name'));
                        }
                    }
                })
                failedInputs = jQuery.uniqueSort(failedInputs);
                jQuery('.error-list').remove();
                if (failedInputs.length > 0) {
                    var errors = jQuery('<div>', {class: 'error-list'});
                    var errorList = jQuery('<ul>', {class: 'item-list__comma-list'})
                    if (failedInputs.length === 1) {
                        errors.text('1 Fehler wurde gefunden')
                    } else {
                        errors.text(failedInputs.length+' Fehler wurden gefunden')
                    }
                    errors.append(errorList);
                    jQuery.each(failedInputs, function(k, errorValue) {
                        errorList.append(jQuery('<li>', {text: errorValue}))
                    })
                    innerForm.prepend(errors);
                    jQuery('body, html').animate({
                        scrollTop: errors.offset().top
                    }, 200);
                } else {
                    innerForm[0].submit();
                }
            })
        })
    }
    /**
     * Cookie Banner
     */
    var cookieBanner = jQuery('#cookie-banner');
    if (cookieBanner.length > 0) {
        if (!getCookie('cookie-consent')) {
            cookieBanner.show();
        }
        cookieBanner.find('.accept').click(function() {
            setCookie('cookie-consent', true, 365);
            cookieBanner.hide();
        })
        cookieBanner.find('.cookie-banner__close').click(function() {
            cookieBanner.hide();
        })
    }
    /**
     * Card Contets
     */
    var cardSections = jQuery('.section');
    if (cardSections.length > 0) {
        cardSections.each(function(k, value) {
            var cardContents = jQuery(value).find('.card.is-vertical .card__content');
            imagesLoaded(cardContents, function() {
                var maxHeight = 0;
                cardContents.each(function(k, value) {
                    value = jQuery(value);
                    var h = value.outerHeight();
                    if (h > maxHeight) {
                        maxHeight = h;
                    }
                })
                cardContents.each(function(k, value) {
                    value = jQuery(value);
                    value.css('height', maxHeight);
                })
            })
        })
    }
    var clickCards = jQuery('.card');
    if (clickCards.length > 0) {
        clickCards.each(function(k, value) {
            value = jQuery(value);
            if (value.data('url')) {
                value.click(function(e) {
                    e.preventDefault();
                    window.location = value.data('url');
                })
            }
        })
    }
    var eventWidget = jQuery('.event-widget');
    if (eventWidget.length > 0) {
        var closestRow = eventWidget.closest('.row');
        if (closestRow.children().length > 1) {
            var maxHeight = 610;
            imagesLoaded(closestRow, function() {
                closestRow.children().each(function(k, value) {
                    value = jQuery(value);
                    var valueCard = value.find('.card');
                    if (valueCard.length > 0) {
                        valueCardHeight = valueCard.height();
                        if (valueCardHeight > maxHeight) {
                            maxHeight = valueCardHeight;
                        }
                    }
                    eventWidget.closest('.row').children().each(function(k, value) {
                        value = jQuery(value);
                        var valueCard = value.find('.card');
                        if (valueCard.length > 0) {
                            valueCard.height(maxHeight+5)
                        }
                    });
                });
            })
        }
    }
    /**
     * Calender Filter Nav
     */
    var eventOverviewHeader = jQuery('.event-overview-header');
    if (eventOverviewHeader.length > 0) {
        var weekdaysPrev = jQuery('.weekdays.prev');
        var weekdaysCurrent = jQuery('.weekdays.current');
        var weekdaysNext = jQuery('.weekdays.next');
        eventOverviewHeader.find('.week-nav .prev-week').click(function(e) {
            e.preventDefault();
            weekdaysPrev.show();
            weekdaysCurrent.hide();
            weekdaysNext.hide();
        })
        eventOverviewHeader.find('.week-nav .current-week').click(function(e) {
            e.preventDefault();
            weekdaysPrev.hide();
            weekdaysCurrent.show();
            weekdaysNext.hide();
        })
        eventOverviewHeader.find('.week-nav .next-week').click(function(e) {
            e.preventDefault();
            weekdaysPrev.hide();
            weekdaysCurrent.hide();
            weekdaysNext.show();
        })
    }
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        cvalue = JSON.stringify(cvalue);
        document.cookie = cname + "=" + cvalue + ";" + expires + "; path=/";
    }
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
            c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    /**
     * Check for IE (Better to be done at PHP Level)
     */
    function getInternetExplorerVersion() {
        var rV = -1;
        if (navigator.appName == 'Microsoft Internet Explorer' || navigator.appName == 'Netscape') {
            var uA = navigator.userAgent;
            var rE = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");

            if (rE.exec(uA) != null) {
                rV = parseFloat(RegExp.$1);
            }
            else if (!!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                rV = 11;
            }
        }
        return rV;
    }

});
